package zstack.apicost.analyzer.utils;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.TypeReference;

import java.util.*;

public class Graph {

    private HashMap<String, Integer> sid2gid;
    private HashMap<Integer, String> gid2sid;
    private int[][] graph;

    public Graph(HashSet<String> sidSet) {
        ArrayList<String> ids = new ArrayList<>(sidSet);

        sid2gid = new HashMap<>();
        gid2sid = new HashMap<>();
        graph = new int[ids.size()][ids.size()];

        for (String id: ids) {
            sid2gid.put(id, ids.indexOf(id));
            gid2sid.put(ids.indexOf(id), id);
        }
        for (int[] ints : graph) Arrays.fill(ints, 0);
    }

    /**
     * 用节点字符串id设置边
     */
    public void setEdge(String fromSid, String toSid) {
        this.graph[sid2gid.get(fromSid)][sid2gid.get(toSid)] = 1;
    }

    /**
     * 把图g1加到图graph上
     */
    private static void graphAddG1(Graph graph, Graph g1) {
        for (int i = 0; i < g1.getGraph().length; i++)
            for (int j = 0; j < g1.getGraph()[i].length; j++)
                if (g1.getGraph()[i][j] != 0)
                    graph.setEdge(g1.getGid2sid().get(i), g1.getGid2sid().get(j));
    }

    /**
     * 合并两个图g1,g2
     */
    public static Graph unionGraph(Graph g1, Graph g2) {
        HashSet<String> ids = new HashSet<>(g1.getSid2gid().keySet());
        ids.addAll(g2.getSid2gid().keySet());
        Graph graph = new Graph(ids);
        Graph.graphAddG1(graph, g1);
        Graph.graphAddG1(graph, g2);
        return graph;
    }

    /**
     * 按给定id列表设置边
     */
    public void setGraphBySidList(ArrayList<String> sidList) {
        for (int i = 0; i < sidList.size()-1; i++)
            this.setEdge(sidList.get(i), sidList.get(i+1));
    }

    // 求图上某个结点的子结点
    private ArrayList<Integer> getGraphSubId(int startGid) {
        ArrayList<Integer> subIds = new ArrayList<>();
        for (int i = 0; i < this.getGraph()[startGid].length; i++) {
            if (this.getGraph()[startGid][i] == 1)
                subIds.add(i);
        }
        return subIds;
    }

    // 构造一个带路径的结点
    private JSONObject getNode(Integer curGid, ArrayList<Integer> parentPath) {
        ArrayList<Integer> curPath = new ArrayList<>(parentPath);
        curPath.add(curGid);
        JSONObject node = new JSONObject();
        node.put("id", curGid);
        node.put("path", curPath);
        return node;
    }

    /**
     * 从指定结点，遍历整个图
     * @param startGid < 0 则按 startGid = 0 计算
     * @return 所有路径
     */
    public ArrayList<int[]> getPathList(int startGid) throws Exception {
        startGid = Math.max(startGid, 0);
        JSONObject startNode = this.getNode(startGid, new ArrayList<>());

        Stack<JSONObject> toVisit = new Stack<>();
        toVisit.push(startNode);

        ArrayList<int[]> pathList = new ArrayList<>();
        // 路径里，要把当前起始节点也加上
        ArrayList<Integer> curPath;
        // 深度优先遍历图
        ArrayList<Integer> toAdd;
        while (!toVisit.empty()) {
            JSONObject curNode = toVisit.pop();
            Integer curId = curNode.getInteger("id");
            curPath = curNode.getObject("path", new TypeReference<ArrayList<Integer>>(){});

            toAdd = this.getGraphSubId(curId);
            for (Integer t : toAdd) {
                JSONObject nextNode = this.getNode(t, curPath);
                toVisit.push(nextNode);
            }

            // 判断路径是否成环
            if (new HashSet<>(curPath).size() != curPath.size())
                throw new Exception(String.format("Group contains a loop %s.", curPath.toString()));
            // 走到叶子节点，完成一条路径
            if (toAdd.isEmpty()) {
                int[] path = new int[curPath.size()];
                for (int i = 0; i < curPath.size(); i++)
                    path[i] = curPath.get(i);
                pathList.add(path);
            }
        }
        return pathList;
    }

    /**
     * 返回边的列表
     */
    public ArrayList<String[]> getEdgeList() {
        ArrayList<String[]> edges = new ArrayList<>();
        int[][] graphMatrix = this.getGraph();
        for (int i = 0; i < graphMatrix.length; i++) {
            for (int j = 0; j < graphMatrix[i].length; j++)
                if (graphMatrix[i][j] != 0)
                    edges.add(new String[]{this.getGid2sid().get(i), this.getGid2sid().get(j)});
        }
        return edges;
    }

    public HashMap<String, Integer> getSid2gid() {
        return sid2gid;
    }

    public HashMap<Integer, String> getGid2sid() {
        return gid2sid;
    }

    public int[][] getGraph() {
        return graph;
    }

    public void setSid2gid(HashMap<String, Integer> sid2gid) {
        this.sid2gid = sid2gid;
    }

    public void setGid2sid(HashMap<Integer, String> gid2sid) {
        this.gid2sid = gid2sid;
    }

    public void setGraph(int[][] graph) {
        this.graph = graph;
    }
}
