package zstack.apicost.analyzer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import zstack.apicost.analyzer.configuration.Global;
import zstack.apicost.analyzer.service.*;
import zstack.apicost.analyzer.vo.zstack.Predictor;

@RestController
public class IndexController {

    @Autowired
    private ApiService apiService;

    @Autowired
    private ApiLogService apiLogService;

    @Autowired
    private StepService stepService;

    @Autowired
    private StepLogService stepLogService;

    @Autowired
    private RStepService rStepService;

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot!";
    }

    @RequestMapping("/apicost")
    public String apiCost(
            @RequestParam(value = "apiid")String apiId,
            @RequestParam(value = "stepid")String stepId) {
        String stepWaited = Global.PREDICTOR.predictByMaxPath(apiId, stepId).toString();
        return String.format("当前API：%s，当前步骤：%s。预期还需 %s 秒完成。", apiId, stepId, stepWaited);
    }

}