package zstack.apicost.analyzer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zstack.apicost.analyzer.entity.Step;
import zstack.apicost.analyzer.repositofy.StepRepository;
import zstack.apicost.analyzer.service.StepService;

import java.math.BigDecimal;
import java.util.List;

@Service
public class StepServiceImpl implements StepService {

    @Autowired
    private StepRepository stepRepository;

    @Override
    public List<Step> findAll() {
        return stepRepository.findAll();
    }

    @Override
    public List<Step> listAllStepsByApiId(String apiId) {
        return stepRepository.findByQuery(apiId);
    }

    @Override
    public int updateByStepId(String stepId, Integer logCount, BigDecimal meanWait) {
        return stepRepository.updateByStepId(stepId, logCount, meanWait);
    }

    @Override
    public Step findOne(String stepId) {
        List<Step> result = stepRepository.findByStepId(stepId);
        return result.size() > 0 ? result.get(0) : null;
    }

    @Override
    public Step save(Step step) {
        return stepRepository.save(step);
    }

}
