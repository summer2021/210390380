package zstack.apicost.analyzer.repositofy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import zstack.apicost.analyzer.entity.RStep;

import java.util.List;

public interface RStepRepository extends JpaRepository<RStep, Integer>, JpaSpecificationExecutor<RStep> {

    @Query("SELECT p FROM RStep p where p.apiId = ?1 ")
    List<RStep> findByQuery(String apiId);

    @Transactional
    @Modifying
    @Query("delete from RStep p where p.apiId = ?1 ")
    void deleteByApiId(String apiId);

}
