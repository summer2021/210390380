package zstack.apicost.analyzer.entity;

import com.alibaba.fastjson.annotation.JSONField;
import zstack.apicost.analyzer.common.entity.BaseEntity;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "steplog")
public class StepLog extends BaseEntity<Integer>{

    @Column(name="step_id", length = 255)
    private String stepId;

    @Column(name="name", length = 255)
    private String name;

    @Column(name = "apilog_id")
    private int apiLogId;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "start_time")
    @Temporal(TemporalType.TIMESTAMP)
    private Date startTime;

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getApiLogId() {
        return apiLogId;
    }

    public void setApiLogId(int apiLogId) {
        this.apiLogId = apiLogId;
    }

    public Date getStartTime() {
        return startTime;
    }

    public void setStartTime(Date startTime) {
        this.startTime = startTime;
    }
}
