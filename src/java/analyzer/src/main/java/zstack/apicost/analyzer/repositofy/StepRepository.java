package zstack.apicost.analyzer.repositofy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import zstack.apicost.analyzer.entity.Step;

import java.math.BigDecimal;
import java.util.List;

public interface StepRepository extends JpaRepository<Step, Integer>, JpaSpecificationExecutor<Step> {

    @Query("SELECT p FROM Step p where p.apiId = ?1 ")
    List<Step> findByQuery(String apiId);

    @Transactional
    @Modifying
    @Query("update " +
            "   Step p " +
            "set " +
            "   p.logCount = ?2, " +
            "   p.meanWait = ?3 " +
            "where " +
            "   p.stepId = ?1 ")
    int updateByStepId(String stepId, Integer logCount, BigDecimal meanWait);

    @Query("select p from Step p where p.stepId = ?1")
    List<Step> findByStepId(String stepId);

}
