package zstack.apicost.analyzer.vo.zstack;

import zstack.apicost.analyzer.configuration.Global;
import zstack.apicost.analyzer.entity.Api;
import zstack.apicost.analyzer.entity.ApiLog;
import zstack.apicost.analyzer.service.*;

import java.math.BigDecimal;
import java.util.*;

public class Predictor {

    private HashMap<String, StepGraph> apiGraph;

    private ApiService apiService;
    private ApiLogService apiLogService;
    private StepLogService stepLogService;
    private StepService stepService;
    private RStepService rStepService;

    public Predictor(ApiService apiService,
                      ApiLogService apiLogService,
                      StepLogService stepLogService,
                      StepService stepService,
                      RStepService rStepService) {
        this.apiService = apiService;
        this.apiLogService = apiLogService;
        this.stepService = stepService;
        this.rStepService = rStepService;
        this.stepLogService = stepLogService;
        this.apiGraph = new HashMap<>();

        this.buildStepGraph();
    }

    /**
     * 重建所有api的步骤图
     */
    private void buildStepGraph() {
        // 加载已有的api步骤图
        List<Api> apiList = apiService.findAll();
        StepGraph stepGraph;
        for (Api api: apiList) {
            stepGraph = new StepGraph(api.getApiId(), new HashSet<>(),
                    apiLogService,
                    stepLogService,
                    stepService,
                    rStepService);
            stepGraph.loadTotalGraph();
            this.getApiGraph().put(api.getApiId(), stepGraph);
        }

        // 构建未分析的步骤图
        List<ApiLog> apiLogList = apiLogService.listApiLogsUnAnalyzed();
        for (ApiLog apiLog: apiLogList) {
            String apiId = apiLog.getApiId();
            if (null == apiService.findOne(apiId))
                apiService.save(apiId, apiLog.getName());
            this.getApiGraph().put(apiId, buildAGraph(apiId));
            apiService.updateLastUpdate(apiId, new Date());
            apiLogService.updateIsAnalyzed(apiId, ApiLogService.ANALYZED);
        }
    }



    /**
     * 重建某个api的步骤图
     */
    private StepGraph buildAGraph(String apiId) {
        StepGraph stepGraph = new StepGraph(apiId, new HashSet<>(),
                apiLogService,
                stepLogService,
                stepService,
                rStepService);
        stepGraph.loadTotalGraph();
        stepGraph.rebuildGraph();
        stepGraph.saveTotalGraph();
        return stepGraph;
    }

    /**
     * 通过最大化路径方法，给出预测耗时
     * @return
     */
    public BigDecimal predictByMaxPath(String apiId, String curSid) {
        try {
            StepGraph stepGraph = this.getStepGraph(apiId);
            HashMap<String, Integer> sid2gid = stepGraph.getSid2gid();
            HashMap<Integer, String> gid2sid = stepGraph.getGid2sid();
            ArrayList<int[]> pathList = stepGraph.getPathList(sid2gid.get(curSid));

            ArrayList<BigDecimal> expectWaitedList = new ArrayList<>();
            ArrayList<BigDecimal> expectWaitedPath;
            for (int[] path : pathList) {
                // 从waited里面拿到每个步骤的期望耗时
                expectWaitedPath = new ArrayList<>();
                for (int i : path)
                    expectWaitedPath.add(stepGraph.getStepMap().get(gid2sid.get(i)).getMeanWait());
                // 期望耗时求和
                BigDecimal pathWaited = new BigDecimal("0.0");
                for (BigDecimal e : expectWaitedPath)
                    pathWaited = pathWaited.add(e);
                expectWaitedList.add(pathWaited);
            }

            // 求出最大期望
            BigDecimal maxWaited = expectWaitedList.get(0);
            for (BigDecimal w : expectWaitedList)
                maxWaited = maxWaited.max(w);
            return maxWaited;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    private StepGraph getStepGraph(String apiId) {
        // TODO: 此处请求数据时，才去拿最新的步骤图，是否有更好的方法？
        // 如果api的lastUpdate超时了，就更新，否则就不更新
        Api api = apiService.findOne(apiId);
        boolean needUpdateStepGraph = new Date().getTime() >
                apiService.findOne(apiId).getLastUpdate().getTime() + (1000 * Global.STEP_GRAPH_UPDATE_PERIOD);
        if (needUpdateStepGraph) {
            this.apiGraph.put(apiId, buildAGraph(apiId));
            apiService.updateLastUpdate(apiId, new Date());
        }

        return this.getApiGraph().get(apiId);
    }

    private HashMap<String, StepGraph> getApiGraph() {
        return this.apiGraph;
    }

    public void setApiGraph(HashMap<String, StepGraph> apiGraph) {
        this.apiGraph = apiGraph;
    }
}
