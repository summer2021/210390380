package zstack.apicost.analyzer.entity;

import zstack.apicost.analyzer.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="rstep")
public class RStep extends BaseEntity<Integer> {

    @Column(name = "from_step_id", length = 255)
    private String fromStepId;

    @Column(name = "to_step_id", length = 255)
    private String toStepId;

    @Column(name = "api_id", length = 255)
    private String apiId;

    public String getFromStepId() {
        return fromStepId;
    }

    public void setFromStepId(String fromStepId) {
        this.fromStepId = fromStepId;
    }

    public String getToStepId() {
        return toStepId;
    }

    public void setToStepId(String toStepId) {
        this.toStepId = toStepId;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }
}
