package zstack.apicost.analyzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import zstack.apicost.analyzer.configuration.Global;
import zstack.apicost.analyzer.service.*;
import zstack.apicost.analyzer.vo.zstack.Predictor;

@SpringBootApplication
public class AnalyzerApplication {

    @Autowired
    private ApiService apiService;

    @Autowired
    private ApiLogService apiLogService;

    @Autowired
    private StepLogService stepLogService;

    @Autowired
    private StepService stepService;

    @Autowired
    private RStepService rStepService;

    public static void main(String[] args) {
        SpringApplication.run(AnalyzerApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
        return args -> {
            Global.PREDICTOR = new Predictor(apiService,
                    apiLogService,
                    stepLogService,
                    stepService,
                    rStepService);
        };
    }

}