package zstack.apicost.analyzer.service;

import zstack.apicost.analyzer.entity.Step;

import java.math.BigDecimal;
import java.util.List;

public interface StepService {

    List<Step> findAll();

    List<Step> listAllStepsByApiId(String apiId);

    int updateByStepId(String stepId, Integer logCount, BigDecimal meanWait);

    Step findOne(String stepId);

    Step save(Step step);

}
