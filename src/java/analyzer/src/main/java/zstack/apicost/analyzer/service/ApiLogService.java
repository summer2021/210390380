package zstack.apicost.analyzer.service;

import org.springframework.data.domain.Page;
import zstack.apicost.analyzer.entity.ApiLog;

import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ApiLogService {

    public static int ANALYZED = 1;
    public static int NOT_ANALYZED = 0;

    Page<ApiLog> listApiLogs(Pageable pageable);

    Page<ApiLog> listApiLogsByQuery(String apiId, int isAnalyzed, Pageable pageable);

    List<ApiLog> listApiLogsUnAnalyzed();

    void updateIsAnalyzed(String apiId, int isAnalyzed);

}
