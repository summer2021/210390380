package zstack.apicost.analyzer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zstack.apicost.analyzer.entity.StepLog;
import zstack.apicost.analyzer.repositofy.StepLogRepository;
import zstack.apicost.analyzer.service.StepLogService;

import java.util.List;

@Service
public class StepLogServiceImpl implements StepLogService {

    @Autowired
    private StepLogRepository stepLogRepository;

    @Override
    public Page<StepLog> listApiLogs(Pageable pageable) {
        return stepLogRepository.findAll(pageable);
    }

    @Override
    public List<StepLog> listOrderedStepLogsByApiLogId(Integer apiLogId) {
        return stepLogRepository.findOrderedByApiLogId(apiLogId);
    }

}
