package zstack.apicost.analyzer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import zstack.apicost.analyzer.entity.ApiLog;
import zstack.apicost.analyzer.repositofy.ApiLogRepository;
import zstack.apicost.analyzer.service.ApiLogService;

import java.util.List;

@Service
public class ApiLogServiceImpl implements ApiLogService {

    @Autowired
    private ApiLogRepository apiLogRepository;

    @Override
    public Page<ApiLog> listApiLogs(Pageable pageable) {
        return apiLogRepository.findAll(pageable);
    }

    @Override
    public Page<ApiLog> listApiLogsByQuery(String apiId, int isAnalyzed, Pageable pageable) {
        return apiLogRepository.findByQuery(apiId, isAnalyzed, pageable);
    }

    @Override
    public List<ApiLog> listApiLogsUnAnalyzed() {
        return apiLogRepository.findUnAnalyzed();
    }

    @Override
    public void updateIsAnalyzed(String apiId, int isAnalyzed) {
        apiLogRepository.updateIsAnalyzed(apiId, isAnalyzed);
    }

}
