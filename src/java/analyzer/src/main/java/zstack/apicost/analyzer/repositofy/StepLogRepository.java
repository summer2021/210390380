package zstack.apicost.analyzer.repositofy;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import zstack.apicost.analyzer.entity.StepLog;

import java.util.List;

public interface StepLogRepository extends JpaRepository<StepLog, Integer>, JpaSpecificationExecutor<StepLog> {

    @Query("SELECT p FROM StepLog p where p.apiLogId = ?1 order by p.startTime asc ")
    List<StepLog> findOrderedByApiLogId(Integer apiLogId);

}
