package zstack.apicost.analyzer.entity;

import zstack.apicost.analyzer.common.entity.BaseEntity;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name="step")
public class Step extends BaseEntity<Integer> {

    @Column(name="step_id", length = 255)
    private String stepId;

    @Column(name="name", length = 255)
    private String name;

    @Column(name="mean_wait", columnDefinition = "decimal(19,2)")
    private BigDecimal meanWait;

    @Column(name="api_id", length = 255)
    private String apiId;

    @Column(name="log_count", length = 10)
    private int logCount;

    public String getStepId() {
        return stepId;
    }

    public void setStepId(String stepId) {
        this.stepId = stepId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getMeanWait() {
        return meanWait;
    }

    public void setMeanWait(BigDecimal meanWait) {
        this.meanWait = meanWait;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public int getLogCount() {
        return logCount;
    }

    public void setLogCount(int logCount) {
        this.logCount = logCount;
    }
}
