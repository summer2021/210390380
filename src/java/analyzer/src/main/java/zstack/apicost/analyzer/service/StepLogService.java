package zstack.apicost.analyzer.service;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import zstack.apicost.analyzer.entity.StepLog;

import java.util.List;

public interface StepLogService {

    Page<StepLog> listApiLogs(Pageable pageable);

    List<StepLog> listOrderedStepLogsByApiLogId(Integer apiLogId);

}
