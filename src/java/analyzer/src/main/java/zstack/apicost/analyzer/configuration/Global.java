package zstack.apicost.analyzer.configuration;

import zstack.apicost.analyzer.vo.zstack.Predictor;

public class Global {

    // 系统更新步骤图的时间为1小时一次
    public static final long STEP_GRAPH_UPDATE_PERIOD = 3600;

    public static Predictor PREDICTOR;

}
