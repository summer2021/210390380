package zstack.apicost.analyzer.service;

import zstack.apicost.analyzer.entity.Api;

import java.util.Date;
import java.util.List;

public interface ApiService {

    List<Api> findAll();

    Api findOne(String apiId);

    Api save(String apiId, String name);

    void updateLastUpdate(String apiId, Date lastUpdate);

}
