package zstack.apicost.analyzer.vo.zstack;

import org.springframework.data.domain.PageRequest;
import zstack.apicost.analyzer.entity.ApiLog;
import zstack.apicost.analyzer.entity.RStep;
import zstack.apicost.analyzer.entity.Step;
import zstack.apicost.analyzer.entity.StepLog;
import zstack.apicost.analyzer.service.*;
import zstack.apicost.analyzer.utils.Graph;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

public class StepGraph extends Graph {

    private String apiId;
    private HashMap<String, Step> stepMap;

    private ApiLogService apiLogService;
    private StepLogService stepLogService;
    private StepService stepService;
    private RStepService rStepService;

    StepGraph(String apiId, HashSet<String> sidSet,
              ApiLogService apiLogService,
              StepLogService stepLogService,
              StepService stepService,
              RStepService rStepService) {
        super(sidSet);

        this.apiLogService = apiLogService;
        this.stepService = stepService;
        this.rStepService = rStepService;
        this.stepLogService = stepLogService;

        this.apiId = apiId;
        this.stepMap = new HashMap<>();
    }

    /**
     * 构建一个apiLog的步骤图
     */
    private StepGraph buildGraphBySteps(ApiLog apiLog) {
        // 取出给定apiLog的所有stepLog
        List<StepLog> stepLogs = stepLogService.listOrderedStepLogsByApiLogId(apiLog.getId());

        HashSet<String> sidSet = new HashSet<>();
        for (StepLog stepLog: stepLogs)
            sidSet.add(stepLog.getStepId());

        StepGraph graph = new StepGraph(apiLog.getApiId(), sidSet,
                apiLogService,
                stepLogService,
                stepService,
                rStepService);
        ArrayList<String> sortedSidList = new ArrayList<>();
        for (StepLog stepLog: stepLogs)
            sortedSidList.add(stepLog.getStepId());
        graph.setGraphBySidList(sortedSidList);

        return graph;
    }

    // 更新本次apiLog中包含的需要更新的step
    private void flushStepByApiLog(ApiLog apiLog) {
        // 取出该apiLogId的所有stepLog，把要更新的step放到needUpdate里
        List<StepLog> stepLogs = stepLogService.listOrderedStepLogsByApiLogId(apiLog.getId());

        for (int i = 0; i < stepLogs.size(); i++) {
            StepLog stepLog = stepLogs.get(i);
            StepLog nextStepLog = i + 1 == stepLogs.size() ? stepLog : stepLogs.get(i + 1);
            // 本次stepLog的等待时间
            BigDecimal stepWait = new BigDecimal(nextStepLog.getStartTime().getTime() - stepLog.getStartTime().getTime())
                    .divide(new BigDecimal("1000"), 6, BigDecimal.ROUND_HALF_UP);

            String stepId = stepLog.getStepId();
            if (this.getStepMap().containsKey(stepId)) {
                // 计算meanWait和logCount
                BigDecimal meanWait = this.getStepMap().get(stepId).getMeanWait();
                BigDecimal logCount = new BigDecimal(this.getStepMap().get(stepId).getLogCount());
                BigDecimal newLogCount = logCount.add(new BigDecimal("1"));
                BigDecimal newMeanWait = meanWait.multiply(logCount).add(stepWait).divide(newLogCount, 6, BigDecimal.ROUND_HALF_UP);
                // 更新meanWait和logCount
                this.getStepMap().get(stepId).setMeanWait(newMeanWait);
                this.getStepMap().get(stepId).setLogCount(newLogCount.intValue());
            } else {
                Step step = new Step();
                step.setStepId(stepLog.getStepId());
                step.setLogCount(1);
                step.setMeanWait(stepWait);
                step.setApiId(apiId);
                step.setName(stepLog.getName());
                this.getStepMap().put(stepId, step);
            }
        }
    }

    /**
     * 更新步骤总图
     */
    public void rebuildGraph() {
        int pageNumber = 0;
        int pageSize = 50;
        List<ApiLog> apiLogs = apiLogService.listApiLogsByQuery(this.getApiId(),
                ApiLogService.NOT_ANALYZED, PageRequest.of(pageNumber, pageSize)).toList();

        this.loadTotalGraph();
        StepGraph stepGraph;
        Graph graph;
        while (apiLogs.size() != 0) {
            for (ApiLog apiLog : apiLogs) {
                stepGraph = this.buildGraphBySteps(apiLog);
                // 合并新图graph到旧图totalGraph上
                graph = Graph.unionGraph(this, stepGraph);
                this.setGid2sid(graph.getGid2sid());
                this.setSid2gid(graph.getSid2gid());
                this.setGraph(graph.getGraph());
                // 更新step
                this.flushStepByApiLog(apiLog);
            }

            // 查询下一批次的未分析的apiLogs
            apiLogs = apiLogService.listApiLogsByQuery(this.getApiId(), ApiLogService.NOT_ANALYZED,
                    PageRequest.of(pageSize, ++pageNumber)).toList();
        }
    }

    public void saveTotalGraph() {
        // 更新或创建step
        for (Step step : this.getStepMap().values()) {
            if (null != stepService.findOne(step.getStepId()))
                stepService.updateByStepId(step.getStepId(), step.getLogCount(), step.getMeanWait());
            else {
                System.out.println(step.getMeanWait());
                stepService.save(step);
            }
        }

        // 删除以前的关系，保存新的关系
        rStepService.deleteByApiId(this.getApiId());
        for (String[] edge : this.getEdgeList())
            rStepService.save(this.getApiId(), edge[0], edge[1]);
    }

    /**
     * 从数据库加载指定的步骤图
     */
    public void loadTotalGraph() {
        // 初始化图
        List<Step> allSteps = stepService.listAllStepsByApiId(apiId);
        HashSet<String> sidSet = new HashSet<>();
        for (Step step: allSteps)
            sidSet.add(step.getStepId());
        StepGraph stepGraph = new StepGraph(apiId, sidSet,
                apiLogService,
                stepLogService,
                stepService,
                rStepService);

        // 加载平均等待时间
        for (Step step: allSteps)
            stepGraph.getStepMap().put(step.getStepId(), step);

        // 加载边
        List<RStep> rSteps = rStepService.listAllRStepByApiId(apiId);
        for (RStep rStep: rSteps)
            stepGraph.setEdge(rStep.getFromStepId(), rStep.getToStepId());

        // 复制到当前对象
        this.setGid2sid(stepGraph.getGid2sid());
        this.setSid2gid(stepGraph.getSid2gid());
        this.setGraph(stepGraph.getGraph());
        this.setStepMap(stepGraph.getStepMap());
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public HashMap<String, Step> getStepMap() {
        return stepMap;
    }

    public void setStepMap(HashMap<String, Step> stepMap) {
        this.stepMap = stepMap;
    }
}
