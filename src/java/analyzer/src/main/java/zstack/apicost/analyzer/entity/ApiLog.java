package zstack.apicost.analyzer.entity;

import zstack.apicost.analyzer.common.entity.BaseEntity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "apilog")
public class ApiLog extends BaseEntity<Integer> {

    @Column(name = "is_analyzed", length = 1)
    private int isAnalyzed;

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "api_id", length = 255)
    private String apiId;

    public int getIsAnalyzed() {
        return isAnalyzed;
    }

    public void setIsAnalyzed(int analyzed) {
        this.isAnalyzed = analyzed;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }
}
