package zstack.apicost.analyzer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zstack.apicost.analyzer.entity.Api;
import zstack.apicost.analyzer.repositofy.ApiRepository;
import zstack.apicost.analyzer.service.ApiService;

import java.util.Date;
import java.util.List;

@Service
public class ApiServiceImpl implements ApiService {

    @Autowired
    private ApiRepository apiRepository;

    @Override
    public List<Api> findAll() {
        return apiRepository.findAll();
    }

    @Override
    public Api findOne(String apiId) {
        List<Api> result = apiRepository.findByApiId(apiId);
        return result.size() > 0 ? result.get(0) : null;
    }

    @Override
    public Api save(String apiId, String name) {
        Api api = new Api();
        api.setApiId(apiId);
        api.setName(name);
        return apiRepository.save(api);
    }

    @Override
    public void updateLastUpdate(String apiId, Date lastUpdate) {
        apiRepository.updateLastUpdate(apiId, lastUpdate);
    }

}
