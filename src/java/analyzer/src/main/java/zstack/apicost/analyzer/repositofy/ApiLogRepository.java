package zstack.apicost.analyzer.repositofy;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import zstack.apicost.analyzer.entity.ApiLog;

import java.util.List;

public interface ApiLogRepository extends JpaRepository<ApiLog, Integer>, JpaSpecificationExecutor<ApiLog> {

    @Query("SELECT p FROM ApiLog p where p.apiId = ?1 and p.isAnalyzed = ?2 ")
    Page<ApiLog> findByQuery(String apiId, int isAnalyzed, Pageable pageable);

    @Query("SELECT p FROM ApiLog p where p.isAnalyzed = 0 ")
    List<ApiLog> findUnAnalyzed();

    @Transactional
    @Modifying
    @Query("update ApiLog p set p.isAnalyzed = ?2 where p.apiId = ?1 ")
    void updateIsAnalyzed(String apiId, int isAnalyzed);

}
