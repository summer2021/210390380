package zstack.apicost.analyzer.service;

import zstack.apicost.analyzer.entity.RStep;

import java.util.List;

public interface RStepService {

    List<RStep> findAll();

    List<RStep> listAllRStepByApiId(String apiId);

    RStep save(RStep s);

    RStep save(String apiId, String fromStepId, String toStepId);

    void deleteByApiId(String apiId);

}
