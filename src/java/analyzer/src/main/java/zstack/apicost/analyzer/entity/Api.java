package zstack.apicost.analyzer.entity;

import zstack.apicost.analyzer.common.entity.BaseEntity;
import com.alibaba.fastjson.annotation.JSONField;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Entity
@Table(name = "api")
public class Api extends BaseEntity<Integer> {

    @Column(name = "name", length = 255)
    private String name;

    @Column(name = "api_id", length = 255)
    private String apiId;

    @JSONField(format = "yyyy-MM-dd HH:mm:ss")
    @Column(name = "last_update")
    @Temporal(TemporalType.TIMESTAMP)
    private Date lastUpdate;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getApiId() {
        return apiId;
    }

    public void setApiId(String apiId) {
        this.apiId = apiId;
    }

    public Date getLastUpdate() {
        return lastUpdate;
    }

    public void setLastUpdate(Date lastUpdate) {
        this.lastUpdate = lastUpdate;
    }
}
