package zstack.apicost.analyzer.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import zstack.apicost.analyzer.entity.RStep;
import zstack.apicost.analyzer.repositofy.RStepRepository;
import zstack.apicost.analyzer.service.RStepService;

import java.util.List;

@Service
public class RStepServiceImpl implements RStepService {

    @Autowired
    private RStepRepository rStepRepository;

    @Override
    public List<RStep> findAll() {
        return rStepRepository.findAll();
    }

    @Override
    public List<RStep> listAllRStepByApiId(String apiId) {
        return rStepRepository.findByQuery(apiId);
    }

    @Override
    public RStep save(RStep s) {
        return rStepRepository.save(s);
    }

    @Override
    public RStep save(String apiId, String fromStepId, String toStepId) {
        RStep rStep = new RStep();
        rStep.setApiId(apiId);
        rStep.setFromStepId(fromStepId);
        rStep.setToStepId(toStepId);
        return this.save(rStep);
    }

    @Override
    public void deleteByApiId(String apiId) {
        rStepRepository.deleteByApiId(apiId);
    }

}
