package zstack.apicost.analyzer.repositofy;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;
import zstack.apicost.analyzer.entity.Api;

import java.util.Date;
import java.util.List;

public interface ApiRepository extends JpaRepository<Api, Integer>, JpaSpecificationExecutor<Api> {

    @Query("select p from Api p where p.apiId = ?1 ")
    List<Api> findByApiId(String apiId);

    @Transactional
    @Modifying
    @Query("update Api p set p.lastUpdate = ?2 where p.apiId = ?1")
    void updateLastUpdate(String apiId, Date lastUpdate);
}
