/*
Navicat MySQL Data Transfer

Source Server         : zstack-test
Source Server Version : 50730
Source Host           : 47.108.218.183:7408
Source Database       : apicost

Target Server Type    : MYSQL
Target Server Version : 50730
File Encoding         : 65001

Date: 2021-06-12 12:33:44
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for api
-- ----------------------------
DROP TABLE IF EXISTS `api`;
CREATE TABLE `api` (
  `id` int(10) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `api_id` varchar(255) DEFAULT NULL,
  `last_update` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of api
-- ----------------------------

-- ----------------------------
-- Table structure for apilog
-- ----------------------------
DROP TABLE IF EXISTS `apilog`;
CREATE TABLE `apilog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `api_id` varchar(255) DEFAULT NULL,
  `is_analyzed` int(1) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of apilog
-- ----------------------------
INSERT INTO `apilog` VALUES ('1', 'CreateVmInstance', 'api_1', '0');
INSERT INTO `apilog` VALUES ('2', 'CreateVmInstance', 'api_1', '0');
INSERT INTO `apilog` VALUES ('3', 'CreateVmInstance', 'api_1', '0');
INSERT INTO `apilog` VALUES ('4', 'CreateVmInstance', 'api_1', '0');

-- ----------------------------
-- Table structure for hibernate_sequence
-- ----------------------------
DROP TABLE IF EXISTS `hibernate_sequence`;
CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of hibernate_sequence
-- ----------------------------
INSERT INTO `hibernate_sequence` VALUES ('1');

-- ----------------------------
-- Table structure for rstep
-- ----------------------------
DROP TABLE IF EXISTS `rstep`;
CREATE TABLE `rstep` (
  `from_step_id` varchar(255) DEFAULT NULL,
  `to_step_id` varchar(255) DEFAULT NULL,
  `api_id` varchar(255) DEFAULT NULL,
  `id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of rstep
-- ----------------------------

-- ----------------------------
-- Table structure for step
-- ----------------------------
DROP TABLE IF EXISTS `step`;
CREATE TABLE `step` (
  `id` int(10) NOT NULL,
  `step_id` varchar(255) DEFAULT NULL,
  `name` varchar(255) DEFAULT NULL,
  `mean_wait` decimal(19,2) DEFAULT NULL,
  `api_id` varchar(255) DEFAULT NULL,
  `log_count` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of step
-- ----------------------------

-- ----------------------------
-- Table structure for steplog
-- ----------------------------
DROP TABLE IF EXISTS `steplog`;
CREATE TABLE `steplog` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `step_id` varchar(255) NOT NULL,
  `start_time` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `name` varchar(255) DEFAULT NULL,
  `apilog_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of steplog
-- ----------------------------
INSERT INTO `steplog` VALUES ('1', 'stepA', '2021-06-07 03:49:57', 'prepare-l2-network', '1');
INSERT INTO `steplog` VALUES ('2', 'stepB', '2021-06-07 03:50:01', 'check-ssh-port', '1');
INSERT INTO `steplog` VALUES ('3', 'stepD', '2021-06-07 03:50:08', 'setup-dhcp-server', '1');
INSERT INTO `steplog` VALUES ('4', 'stepE', '2021-06-07 03:50:10', 'install-agent', '1');
INSERT INTO `steplog` VALUES ('5', 'stepA', '2021-06-07 03:50:14', 'prepare-l2-network', '2');
INSERT INTO `steplog` VALUES ('6', 'stepD', '2021-06-07 03:50:19', 'setup-dhcp-server', '2');
INSERT INTO `steplog` VALUES ('7', 'stepE', '2021-06-07 03:50:20', 'install-agent', '2');
INSERT INTO `steplog` VALUES ('8', 'stepA', '2021-06-07 03:50:24', 'prepare-l2-network', '3');
INSERT INTO `steplog` VALUES ('9', 'stepC', '2021-06-07 03:50:34', 'setup-ip-tables', '3');
INSERT INTO `steplog` VALUES ('10', 'stepE', '2021-06-07 03:50:37', 'install-agent', '3');
INSERT INTO `steplog` VALUES ('11', 'stepA', '2021-06-07 03:50:39', 'prepare-l2-network', '4');
INSERT INTO `steplog` VALUES ('12', 'stepC', '2021-06-07 03:50:45', 'setup-ip-tables', '4');
INSERT INTO `steplog` VALUES ('13', 'stepF', '2021-06-07 03:50:46', 'setup-ip-tables-2', '4');
