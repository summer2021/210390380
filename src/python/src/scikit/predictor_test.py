from sklearn.linear_model import LogisticRegression
from sklearn.neighbors import KNeighborsClassifier
from sklearn.ensemble import RandomForestRegressor
from sklearn.tree import DecisionTreeClassifier
import tags


def logistic_regression_predictor(X, y):
    return LogisticRegression(random_state=0).fit(X, y)


def k_neighbors_predictor(X, y):
    return KNeighborsClassifier(n_neighbors=3).fit(X, y)


def random_forest_predictor(X, y):
    return RandomForestRegressor(max_depth=2, random_state=0).fit(X, y)


def decision_tree_predictor(X, y):
    return DecisionTreeClassifier(random_state=0).fit(X, y)


def print_predict(clf, X, y):
    print('---------------------------------------------------------------------')
    print([tags.tags_time(t) for t in clf.predict(X[:20, :])])
    print([tags.tags_time(t) for t in clf.predict_proba(X[:20, :])])
    print(clf.score(X, y))


if __name__ == '__main__':
    '''数据少就用决策树'''
    pass
