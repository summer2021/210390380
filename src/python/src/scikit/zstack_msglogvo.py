import sample
import json
import numpy as np
import load_data
import step_vec
import predictor_test
from sklearn2pmml import PMMLPipeline, sklearn2pmml


def load_and_parse_data(_api_name):
    # load params
    objs = dict()
    df = load_data.load_data_api_params(_api_name)
    for row in df.iterrows():
        objs[row[1]['apiId']] = json.loads(row[1]['requestDump'])
    objs = sample.open_objs(objs)
    objs = sample.normalize_objs(objs)

    # load steps
    load_result = load_data.load_data_api_steps(_api_name)
    step_vectors = step_vec.parse_step_vec(load_result['api_id_steps'], load_result['step_map'])

    # 没有步骤或请求参数的，不进行预测
    if len(step_vectors) == 0 or len(objs) == 0:
        return []

    # union steps and params
    for v in step_vectors:
        api_id = v['api_id']
        param_obj = objs[api_id]
        step_vector_obj = v['step_vec']
        for k in param_obj:
            step_vector_obj[k] = param_obj[k]
    return step_vectors


def train_save_model(_api_name):
    _data = load_and_parse_data(_api_name)
    if len(_data) == 0:
        return

    _objs = []
    _need_time = []
    _need_steps = []
    for _d in _data:
        _objs += [_d['step_vec']]
        _need_time += [_d['need_time']]
        _need_steps += [_d['need_steps']]

    keys = []
    for obj in _objs:
        keys += obj.keys()
    keys = sorted(set(keys))
    print('[{}] keys is: {}'.format(_api_name, keys))
    if len(keys) == 0:
        return

    X = []
    for obj in _objs:
        Xi = [obj[k] if k in sorted(obj.keys()) else 0 for k in keys]
        X += [Xi]
    X = np.array(X).astype(float)
    y_need_steps = np.array([w for w in _need_steps])
    y_need_time = np.array([w for w in _need_time])

    # train need steps
    clf_steps = predictor_test.k_neighbors_predictor(X, y_need_steps)
    pipeline_steps = PMMLPipeline([('{}_need_steps'.format(_api_name), clf_steps)])
    pipeline_steps.fit(X, y_need_steps)

    # print(pipeline_steps.predict(X[:31, :]))

    sklearn2pmml(pipeline_steps, './data/{}_steps.pmml'.format(_api_name), with_repr=True)

    # train need time
    clf_time = predictor_test.random_forest_predictor(X, y_need_time)
    pipeline_time = PMMLPipeline([('{}_need_time'.format(_api_name), clf_time)])
    pipeline_time.fit(X, y_need_time)

    print(pipeline_time.predict(X[:31, :]))

    sklearn2pmml(pipeline_time, './data/{}_time.pmml'.format(_api_name), with_repr=True)


if __name__ == '__main__':
    api_names = ['org.zstack.baremetal2.chassis.APIGetBareMetal2SupportedBootModeMsg',
                 'org.zstack.billing.APICalculateAccountSpendingMsg',
                 'org.zstack.guesttools.APIGetVmGuestToolsInfoMsg',
                 'org.zstack.ha.APISetVmInstanceHaLevelMsg',
                 'org.zstack.header.allocator.APIGetCpuMemoryCapacityMsg',
                 'org.zstack.header.APIIsOpensourceVersionMsg',
                 'org.zstack.header.apimediator.APIIsReadyToGoMsg',
                 'org.zstack.header.console.APIRequestConsoleAccessMsg',
                 'org.zstack.header.core.progress.APIGetTaskProgressMsg',
                 'org.zstack.header.host.APIChangeHostStateMsg',
                 'org.zstack.header.identity.APIGetAccountQuotaUsageMsg',
                 'org.zstack.header.identity.APIGetResourceAccountMsg',
                 'org.zstack.header.identity.APILogInByAccountMsg',
                 'org.zstack.header.identity.APILogOutMsg',
                 'org.zstack.header.identity.APIValidateSessionMsg',
                 'org.zstack.header.image.APIGetCandidateBackupStorageForCreatingImageMsg',
                 'org.zstack.header.image.APIGetImageQgaMsg',
                 'org.zstack.header.longjob.APISubmitLongJobMsg',
                 'org.zstack.header.managementnode.APIGetCurrentTimeMsg',
                 'org.zstack.header.managementnode.APIGetManagementNodeArchMsg',
                 'org.zstack.header.managementnode.APIGetPlatformTimeZoneMsg',
                 'org.zstack.header.managementnode.APIGetVersionMsg',
                 'org.zstack.header.network.l3.APIGetFreeIpMsg',
                 'org.zstack.header.network.l3.APIGetIpAddressCapacityMsg',
                 'org.zstack.header.network.l3.APIGetL3NetworkMtuMsg',
                 'org.zstack.header.network.l3.APIGetL3NetworkRouterInterfaceIpMsg',
                 'org.zstack.header.storage.primary.APIGetPrimaryStorageLicenseInfoMsg',
                 'org.zstack.header.storage.snapshot.APIRevertVolumeFromSnapshotMsg',
                 'org.zstack.header.vm.APIAttachIsoToVmInstanceMsg',
                 'org.zstack.header.vm.APIChangeInstanceOfferingMsg',
                 'org.zstack.header.vm.APICreateVmInstanceMsg',
                 'org.zstack.header.vm.APIDestroyVmInstanceMsg',
                 'org.zstack.header.vm.APIDetachIsoFromVmInstanceMsg',
                 'org.zstack.header.vm.APIExpungeVmInstanceMsg',
                 'org.zstack.header.vm.APIGetCandidateIsoForAttachingVmMsg',
                 'org.zstack.header.vm.APIGetCandidateZonesClustersHostsForCreatingVmMsg',
                 'org.zstack.header.vm.APIGetInterdependentL3NetworksImagesMsg',
                 'org.zstack.header.vm.APIGetNicQosMsg',
                 'org.zstack.header.vm.APIGetVmAttachableL3NetworkMsg',
                 'org.zstack.header.vm.APIGetVmBootOrderMsg',
                 'org.zstack.header.vm.APIGetVmConsoleAddressMsg',
                 'org.zstack.header.vm.APIGetVmsCapabilitiesMsg',
                 'org.zstack.header.vm.APIRebootVmInstanceMsg',
                 'org.zstack.header.vm.APISetVmBootOrderMsg',
                 'org.zstack.header.vm.APIStartVmInstanceMsg',
                 'org.zstack.header.vm.APIStopVmInstanceMsg',
                 'org.zstack.header.vm.APIUpdateVmInstanceMsg',
                 'org.zstack.header.vo.APIGetResourceNamesMsg',
                 'org.zstack.header.volume.APIAttachDataVolumeToVmMsg',
                 'org.zstack.header.volume.APICreateDataVolumeMsg',
                 'org.zstack.header.volume.APICreateVolumeSnapshotMsg',
                 'org.zstack.header.volume.APIDeleteDataVolumeMsg',
                 'org.zstack.header.volume.APIDetachDataVolumeFromVmMsg',
                 'org.zstack.header.volume.APIExpungeDataVolumeMsg',
                 'org.zstack.header.volume.APIGetDataVolumeAttachableVmMsg',
                 'org.zstack.header.volume.APIGetVolumeCapabilitiesMsg',
                 'org.zstack.header.volume.APIGetVolumeQosMsg',
                 'org.zstack.iam2.api.APICheckIAM2OrganizationAvailabilityMsg',
                 'org.zstack.iam2.api.APIGetIAM2OrganizationVirtualIDNumberMsg',
                 'org.zstack.iam2.api.APIGetIAM2ProjectsOfVirtualIDMsg',
                 'org.zstack.iam2.api.APILoginIAM2ProjectMsg',
                 'org.zstack.iam2.api.APILoginIAM2VirtualIDMsg',
                 'org.zstack.license.APIGetLicenseAddOnsMsg',
                 'org.zstack.license.APIGetLicenseInfoMsg',
                 'org.zstack.loginControl.api.APIGetLoginCaptchaMsg',
                 'org.zstack.network.securitygroup.APIAttachSecurityGroupToL3NetworkMsg',
                 'org.zstack.network.securitygroup.APICreateSecurityGroupMsg',
                 'org.zstack.network.service.eip.APIAttachEipMsg',
                 'org.zstack.network.service.eip.APIGetEipAttachableVmNicsMsg',
                 'org.zstack.network.service.vip.APICreateVipMsg',
                 'org.zstack.pciDevice.APIGetHostIommuStateMsg',
                 'org.zstack.pciDevice.APIGetHostIommuStatusMsg',
                 'org.zstack.pciDevice.specification.pci.APIGetPciDeviceSpecCandidatesMsg',
                 'org.zstack.resourceconfig.APIGetResourceConfigMsg',
                 'org.zstack.resourceconfig.APIUpdateResourceConfigMsg',
                 'org.zstack.scheduler.APIGetSchedulerExecutionReportMsg',
                 'org.zstack.storage.migration.primary.APIGetPrimaryStorageCandidatesForVolumeMigrationMsg',
                 'org.zstack.storage.primary.local.APIGetLocalStorageHostDiskCapacityMsg',
                 'org.zstack.storage.primary.local.APILocalStorageGetVolumeMigratableHostsMsg',
                 'org.zstack.twoFactorAuthentication.APIGetTwoFactorAuthenticationStateMsg',
                 'org.zstack.zwatch.api.APIGetAlarmDataMsg',
                 'org.zstack.zwatch.api.APIGetAuditDataMsg',
                 'org.zstack.zwatch.api.APIGetEventDataMsg',
                 'org.zstack.zwatch.api.APIGetManagementNodeDirCapacityMsg',
                 'org.zstack.zwatch.api.APIGetMetricDataMsg',
                 'org.zstack.zwatch.api.APIGetMetricLabelValueMsg', ]
    for api in api_names:
        if api not in ['org.zstack.guesttools.APIGetVmGuestToolsInfoMsg',
                       'org.zstack.header.vm.APIAttachIsoToVmInstanceMsg',
                       'org.zstack.header.vm.APICreateVmInstanceMsg',
                       'org.zstack.header.vm.APIDestroyVmInstanceMsg',
                       'org.zstack.header.vm.APIRebootVmInstanceMsg',
                       'org.zstack.header.vm.APIStartVmInstanceMsg',
                       'org.zstack.header.vm.APIStopVmInstanceMsg',
                       'org.zstack.header.volume.APIAttachDataVolumeToVmMsg',
                       'org.zstack.header.volume.APICreateVolumeSnapshotMsg']: continue
        print('train [{}]'.format(api))
        train_save_model(api)
