def time_tags(_time):
    grades = [0.5, 1, 2, 3, 5, 7, 10, 15, 20, 30, 40, 60, 100, 200, 300, 400]
    for i in range(len(grades)):
        if _time < grades[i]:
            return i


def tags_time(_tag):
    grades = [0.5, 1, 2, 3, 5, 7, 10, 15, 20, 30, 40, 60, 100, 200, 300, 400]
    return grades[_tag]


if __name__ == '__main__':
    print(time_tags(20.5))
