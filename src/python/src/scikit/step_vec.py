def parse_step_vec(api_id_steps, step_map):
    '''
    解析步骤向量，返回步骤向量列表，每一行附带所属api_id
    :param api_id_steps:
    :param step_map:
    :return:
    [
        {'step_vec': {'step1': 0.2, ...}, 'need_time': 1.2, 'need_steps': 3, 'api_id': 'fdosjf587484fds'},
        {...},
        ...
        {...}
    ]
    '''
    if len(api_id_steps) == 0:
        return []
    all_objs = []
    for step_obj in api_id_steps:
        # 从第一条开始，把每一条当成当前步骤，算出剩余时间，剩余步数，
        all_objs += build_a_api_row(step_obj['steps'], step_map, step_obj['api_id'])
    return normalize_step_vec(all_objs)


def build_a_api_row(_steps, _step_map, api_id):
    _objs = []
    _api_reply_time = max([s['replyTime'] for s in _steps])
    for i in range(len(_steps)):
        _step = _steps[i]
        _need_time = (_api_reply_time - _step['startTime']) / 1000.0
        _need_steps = len(_steps)
        _step_vec = _step_map.copy()
        for _step in _steps[:i+1]:
            _step_vec[_step['msgName']] += 1
        _objs += [{'step_vec': _step_vec, 'need_time': _need_time, 'need_steps': _need_steps, 'api_id': api_id}]
    return _objs


def normalize_step_vec(_objs):
    '''
    把步骤计数规范化到[0,1]
    :param _objs:
    :return:
    '''
    divisor = max([max(o['step_vec'].values()) for o in _objs])
    for _o in _objs:
        step_vec = _o['step_vec']
        for k in step_vec:
            step_vec[k] /= divisor
    return _objs


if __name__ == '__main__':
    apiName = 'org.zstack.header.vm.APICreateVmInstanceMsg'
    import load_data
    load_result = load_data.load_data_api_steps(apiName)
    objs = parse_step_vec(load_result['api_id_steps'], load_result['step_map'])
    print(objs)
    print(len(objs))
