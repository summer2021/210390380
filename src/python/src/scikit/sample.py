from queue import Queue
import time
import json
import re

# 原始样本数据
objs = []
# 样本量
n = 0
# 每个字符串字段的样本计数map
str_count = dict()
# 每个数字字段的最大最小值map
min_max = dict()


def open_objs(_objs):
    '''
    展开objs
    :param _objs:
    :return:
    '''
    _new_objs = dict()
    for k_api_id in _objs:
        _obj = _objs[k_api_id]
        _new_obj = dict()
        q = Queue(0)
        for k in _obj.keys():
            q.put(k)
        while not q.empty():
            k = q.get(block=False)
            v = _obj[k]
            open_kv(_obj, _new_obj, q, k, v)
        _new_objs[k_api_id] = _new_obj
    return _new_objs


def open_kv(_obj, _new_obj, q, k, v):
    # 数字和字符串直接展开
    if type(v) == int or type(v) == str:
        _new_obj[k] = _obj[k]
    # 时间戳转成结构化时间
    if type(v) == int and v > 0 and 2000 < time.localtime(v / 1000).tm_year < 2099:
        _new_obj[k] = time.localtime(v / 1000)
    elif type(v) == int and v > 0 and 2000 < time.localtime(v).tm_year < 2099:
        _new_obj[k] = time.localtime(v)
    # 展开一个对象
    if type(v) == dict:
        # 对象字段个数
        _new_obj['n_' + k] = len(v.keys())
        # 对象子字段变成待展开，放到展开队列里
        for k1 in v:
            v1 = v[k1]
            k1 = k + '_' + k1
            q.put(k1)
            _obj[k1] = v1
    # 展开一个数组
    if type(v) == list:
        # 数组转成数字
        _new_obj['n_' + k] = len(v)
        # 数组内容按对象展开 TODO: 这种展开方式不合理
        for i in range(len(v)):
            k1 = k + '_' + str(i)
            v1 = v[i]
            _obj[k1] = v1
            open_kv(_obj, _new_obj, q, k1, v1)


def normalize_objs(_objs):
    # 时间取出今日秒数，归一化
    for k in _objs:
        _obj = _objs[k]
        normalize_date(_obj)

    # 对每一个字符串字段，计算频率
    # 对每一个字符串字段，新增三个属性：长度，空格数，非英文字符数
    normalize_str(_objs)

    # 数字取出最大值与最小值，最大最小值当成样本属性，归一化
    normalize_int(_objs)
    return _objs


def normalize_date(_obj):
    '''
    取成一天中的所在时刻，并归一化，如：2021年9月1日 22:04:23 -> 22:04:23 -> (22*3600+4*60+23)/(24*3600) -> 0.9197106481481482
    :param _obj:
    :return:
    '''
    date_keys = [k for k in _obj.keys() if type(_obj[k]) == time.struct_time]
    for k in date_keys:
        t = _obj[k]
        _obj[k] = (t.tm_hour*3600.0 + t.tm_min*60.0 + t.tm_sec) / (24*3600.0)
    return _obj


def str_count_map(_objs):
    '''
    对每个字符串字段的样本计数，做成map
    :param _objs:
    :return:
    '''
    count_map = dict()
    for k_api_id in _objs:
        _obj = _objs[k_api_id]
        str_keys = [k for k in _obj.keys() if type(_obj[k]) == str]
        for k in str_keys:
            v = _obj[k]
            if k not in count_map:
                count_map[k] = dict()
                count_map[k][v] = 1
            elif k in count_map and v not in count_map[k]:
                count_map[k][v] = 1
            else:
                count_map[k][v] += 1
    return count_map


def normalize_str(_objs):
    '''
    对每一个字符串字段，新增三个属性：长度，空格数，非英文字符数
    :param _objs:
    :return:
    '''
    count_map = str_count_map(_objs)
    for k_api_id in _objs:
        _obj = _objs[k_api_id]
        str_keys = [k for k in _obj.keys() if type(_obj[k]) == str]
        for k in str_keys:
            v = _obj[k]
            num_k = sum([count_map[k][vi] for vi in count_map[k].keys()])
            _obj[k] = float(count_map[k][v] / num_k)
            _obj['length_'+k] = len(v)
            _obj['space_'+k] = len(re.compile(' ').findall(v))
            _obj['mark_'+k] = len(''.join(re.compile('[^a-zA-Z0-9]+').findall(v)))
    return _objs


def int_count_map(_objs):
    '''
    计算每个数字字段的最大最小值，做成map
    :param _objs:
    :return:
    '''
    count_map = dict()
    for k_api_id in _objs:
        _obj = _objs[k_api_id]
        int_keys = [k for k in _obj.keys() if type(_obj[k]) == int]
        for k in int_keys:
            v = _obj[k]
            if k not in count_map:
                count_map[k] = dict()
                count_map[k]['min'] = v
                count_map[k]['max'] = v
            else:
                count_map[k]['min'] = v if v < count_map[k]['min'] else count_map[k]['min']
                count_map[k]['max'] = v if v > count_map[k]['max'] else count_map[k]['max']
    return count_map


def normalize_int(_objs):
    '''
    数字：在[min,max]中归一化，如：样本中此字段最大值10，最小值1，给定样本值 5 -> (5-1)/(10-1) -> 0.4444444444444444
    :param _objs:
    :return:
    '''
    count_map = int_count_map(_objs)
    for k_api_id in _objs:
        _obj = _objs[k_api_id]
        int_keys = [k for k in _obj.keys() if type(_obj[k]) == int]
        for k in int_keys:
            v = _obj[k]
            if count_map[k]['max'] - v == count_map[k]['min']:
                _obj[k] = 1.0
            else:
                _obj[k] = (v - count_map[k]['min'])*1.0 / (count_map[k]['max'] - v - count_map[k]['min'])
    return _objs


if __name__ == '__main__':
    obj = {"namespace": "ZStack/VM", "metricName": "CPUAverageUsedUtilization", "startTime": 1629882489,
           "endTime": 1629883389, "period": 3, "labels": ["VMUuid\u003dc15eee7783fc40288c00292fa18f348e"],
           "labelList": [],
           "valueConditionList": [],
           "session": {"uuid": "98357e4835f34357b204622d04580496", "accountUuid": "305336db288a4e16a9e0f87d4f13182c",
                       "userUuid": "4a817fdd6a314ca5b0b45ed5551e1d33", "expiredDate": "Aug 26, 2021 1:23:09 PM",
                       "createDate": "Aug 25, 2021 5:22:58 PM", "noSessionEvaluation": False}, "timeout": 300000,
           "headers": {"correlationId": "0239bf7e4020421c98504752a224ac43",
                       "replyTo": "84c09da047763b419ca0f3367215b7c1:::cloudbus.messages", "noReply": "false",
                       "thread-context": {}}, "id": "0239bf7e4020421c98504752a224ac43", "createdTime": 1629883389291}
    objs = open_objs([obj])
    objs = normalize_objs(objs)
    print(json.dumps(objs, ensure_ascii=False))
