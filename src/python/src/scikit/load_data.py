from sqlalchemy import create_engine
import pandas as pd


def load_data_api_steps(_api_name):
    '''
    从数据库导出数据，返回{'api_id_steps': (api_id的列表，每一行附带该api_id的步骤列表), 'step_map': (以步骤名为键的map)}
    :param _api_name:
    :return:
    '''
    engine = create_engine('mysql+pymysql://root:Zp8TkQfZGn8JMIas@47.108.248.105:3306/zstack')

    # load这个apiName所有的apiId
    sql = ('SELECT apiId FROM `MsgLogVO` where taskName = "{}" group by apiId'.format(_api_name))
    api_ids = pd.read_sql_query(sql, engine)

    # load这个apiName所有的step
    sql = ('SELECT msgName FROM `MsgLogVO` where taskName = "{}" group by msgName'.format(_api_name))
    api_step_names = pd.read_sql_query(sql, engine)
    step_map = dict([(s[1]['msgName'], 0) for s in api_step_names.iterrows()])

    # load这个apiId的数据，按照startTime排序
    api_id_steps = []
    for row in api_ids.iterrows():
        api_id = row[1]['apiId']
        sql = ('SELECT apiId, msgName, startTime, replyTime FROM `MsgLogVO` '
               'where taskName = "{}" and apiId = "{}" order by startTime'.format(_api_name, api_id))
        steps = [s[1] for s in pd.read_sql_query(sql, engine).iterrows()]
        if len(steps) > 3:
            api_id_steps += [{'steps': steps, 'api_id': api_id}]
    return {'api_id_steps': api_id_steps, 'step_map': step_map}


def load_data_api_params(_api_name):
    engine = create_engine('mysql+pymysql://root:Zp8TkQfZGn8JMIas@47.108.248.105:3306/zstack')
    sql = ('select '
           '    t1.*, t2.requestDump '
           'from '
           '    (select '
           '        max(wait) mwait, count(1) step, apiId, taskName '
           '    from '
           '        MsgLogVO '
           '    where '
           '        taskName = "{}" '
           '    group by '
           '        apiId '
           '    order by '
           '        mwait desc) as t1 inner join '
           '    (select '
           '        * '
           '    from '
           '        APIHistoryVO '
           '    where '
           '        apiName = "{}") as t2 '
           'where '
           '    t1.apiId = t2.requestUuid '
           'limit 3000 '.format(_api_name, _api_name))
    return pd.read_sql_query(sql, engine)
