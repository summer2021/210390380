def get_sub_steps(_steps):
    '''
    递归提取子步骤
    :param _steps:
    :return:
    '''
    if type(_steps) is list:
        return _steps
    if 'sub-steps' not in _steps or len(_steps['sub-steps']) == 0:
        return [_steps]
    _all_sub_steps = [_steps]
    for _s in _steps['sub-steps']:
        _all_sub_steps += get_sub_steps(_s)
    return _all_sub_steps


def steps_normalize(_api_data):
    '''
    规范化步骤
    :return:
    '''
    # 遍历steps使其线性化
    _steps = []
    for _s in _api_data['steps']:
        _steps += get_sub_steps(_s)
    # 按start-time排序
    _steps.sort(key=lambda _s: _s['start-time'])
    return _steps


if __name__ == '__main__':
    import test_data
    steps1 = steps_normalize(test_data.api_data1)
    for s in steps1:
        print(s)
