def get_group_structure(_ids):
    '''
    按id提取顶点列表，生成空白二维矩阵，定义矩阵下标与id的映射
    :return:
    '''
    # 按id提取顶点列表，生成空白二维矩阵，定义矩阵下标与id的映射
    len_vertex = len(_ids)
    return {
        'sid_gid': dict([(_k, _ids.index(_k)) for _k in _ids]),
        'gid_sid': dict([(_ids.index(_k), _k) for _k in _ids]),
        'group': [[0 for i in range(len_vertex)] for i in range(len_vertex)]
    }


def get_adjacent(_steps):
    '''
    生成邻接矩阵
    :return:
    '''
    # 按线性化的steps填充邻接矩阵
    _ids = [_s['id'] for _s in _steps]
    _ids = list(set(_ids))
    _group = get_group_structure(_ids)
    for _i in range(len(_steps) - 1):
        _s = _steps[_i]
        _to_s = _steps[_i + 1]
        _from_gid = _group['sid_gid'][_s['id']]
        _to_gid = _group['sid_gid'][_to_s['id']]
        _group['group'][_from_gid][_to_gid] = 1
    return _group


def add_group1_to_group(_group1, _group):
    '''
    把_group1合并到_group
    :param _group1:
    :param _group:
    :return:
    '''
    for _i in range(len(_group1['group'])):
        for _j in range(len(_group1['group'][_i])):
            if _group1['group'][_i][_j] == 1:
                _from_sid = _group1['gid_sid'][_i]
                _to_sid = _group1['gid_sid'][_j]
                _new_from_gid = _group['sid_gid'][_from_sid]
                _new_to_gid = _group['sid_gid'][_to_sid]
                _group['group'][_new_from_gid][_new_to_gid] = 1
    return _group


def union_adjacent(_group1, _group2):
    '''
    合并两个图
    :param _group1:
    :param _group2:
    :return:
    '''
    _ids1 = [_s for _s in _group1['sid_gid']]
    _ids2 = [_s for _s in _group2['sid_gid']]
    _ids = list(set(_ids1 + _ids2))
    _group = get_group_structure(_ids)
    _group = add_group1_to_group(_group1, _group)
    _group = add_group1_to_group(_group2, _group)
    return _group


if __name__ == '__main__':
    import test_data, steps
    steps1 = steps.steps_normalize(test_data.api_data1)
    for s in steps1:
        print(s)

    group1 = get_adjacent(steps1)
    for k in group1:
        if k != 'group':
            print('{}: {}'.format(k, group1[k]))
        else:
            [print(_i) for _i in group1[k]]
