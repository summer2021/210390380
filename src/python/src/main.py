import gen_group
import steps
import wait_predict


if __name__ == '__main__':
    import test_data
    steps1 = steps.steps_normalize(test_data.api_data1)
    group1 = gen_group.get_adjacent(steps1)
    steps2 = steps.steps_normalize(test_data.api_data2)
    group2 = gen_group.get_adjacent(steps2)
    group = gen_group.union_adjacent(group1, group2)
    # 等待时间
    waited_history = wait_predict.steps_waited_history([test_data.api_data1, test_data.api_data2])
    waited_expect = wait_predict.steps_waited_expect_by_mean(waited_history)
    for k in waited_expect:
        print('{}: {}'.format(k, waited_expect[k]))
    for _g in group['gid_sid']:
        print('------------------------start: {}--------------------------'.format(group['gid_sid'][_g]))
        if group['gid_sid'][_g] == 'stepA1':
            print('hello')
        paths = wait_predict.path_list(group['group'], int(_g))
        for p in paths:
            print('path: {}'.format([group['gid_sid'][_i] for _i in p]))
            print('{}: {}'.format(group['gid_sid'][_g], wait_predict.predict_path_waited(p, group['gid_sid'], waited_expect)))

    # 用邻接矩阵画出有向无环图
    import networkx as nx
    import matplotlib.pyplot as plt

    group_matrix = group['group']
    label_dict = group['gid_sid']
    G = nx.DiGraph()
    for i in range(len(group_matrix)):
        for j in range(len(group_matrix)):
            if group_matrix[i][j]:
                G.add_edge(i, j)
    nx.draw_spectral(G, labels=label_dict, with_labels=True, font_color='r')
    plt.show()
