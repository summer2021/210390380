import steps


def steps_waited_history(_api_datas):
    '''
    节点历史时间，期望时间
    :param _api_datas:
    :return:
    '''
    _waited = dict()
    for _api_data in _api_datas:
        # steps线性化
        _steps_i = steps.steps_normalize(_api_data)
        _steps_misplace_i = _steps_i + [{'start-time': _steps_i[-1]['start-time']}]
        # 逐项求差，与step配对
        _waited_i = dict([(_s['id'], _sm['start-time'] - _s['start-time'])
                          for (_s, _sm) in zip(_steps_i, _steps_misplace_i[1:])])
        # 加入节点历史时间
        for _id in _waited_i:
            if _id in _waited:
                _waited[_id] += [_waited_i[_id]]
            else:
                _waited[_id] = [_waited_i[_id]]
    return _waited


def steps_waited_expect_by_mean(_waited):
    '''
    平均值法求步骤期望耗时
    :param _waited:
    :return:
    '''
    return dict([(_i, sum(_waited[_i])/len(_waited[_i])) for _i in _waited])


def get_group_sub_id(_group_matrix, _cur_id):
    '''
    求图上节点的子节点
    :param _group_matrix:
    :param _cur_id:
    :return:
    '''
    _sub_ids = []
    for _i in range(len(_group_matrix[_cur_id])):
        if _group_matrix[_cur_id][_i] == 1:
            _sub_ids.append(_i)
    return _sub_ids


def path_list(_group_matrix, _start_id):
    '''
    给定开始节点，遍历有向无环图
    注意随时判断环，截断它，记录路径，深度优先遍历
    :param _group_matrix:
    :param _start_id:
    :return:
    '''
    if type(_start_id) != int or _start_id < 0:
        raise Exception('Wrong start_id.')
    _to_visit = get_group_sub_id(_group_matrix, _start_id)
    _path_list = []
    # 路径里，要把当前起始节点也加上
    _cur_path = [_start_id]
    # 深度优先遍历图
    while len(_to_visit):
        _cur_i = _to_visit.pop()
        _cur_path += [_cur_i]
        _to_add = get_group_sub_id(_group_matrix, _cur_i)
        _to_visit += _to_add
        # 判断路径是否成环
        if len(set(_cur_path)) != len(_cur_path):
            raise Exception('Group contains a loop {}.'.format(_cur_path))
        # 走到叶子节点，完成一条路径
        if not len(_to_add):
            _path_list += [_cur_path]
            _cur_path = [_start_id]
    return _path_list


def predict_path_waited(_path, _gid_sid, _expect_waited):
    '''
    通过期望时间计算路径耗时
    :param _path: 路径节点id，邻接矩阵id
    :param _gid_sid: 邻接矩阵id转步骤id
    :param _expect_waited: 步骤期望耗时
    :return:
    '''
    return sum([_expect_waited[_gid_sid[_i]] for _i in _path])


if __name__ == '__main__':
    import test_data, gen_group
    steps1 = steps.steps_normalize(test_data.api_data1)
    group1 = gen_group.get_adjacent(steps1)
    steps2 = steps.steps_normalize(test_data.api_data2)
    group2 = gen_group.get_adjacent(steps2)
    group = gen_group.union_adjacent(group1, group2)
    # 等待时间
    waited_history = steps_waited_history([test_data.api_data1, test_data.api_data2])
    waited_expect = steps_waited_expect_by_mean(waited_history)
    for k in waited_expect:
        print('{}: {}'.format(k, waited_expect[k]))
    for _g in group['gid_sid']:
        print('------------------------start: {}--------------------------'.format(group['gid_sid'][_g]))
        if group['gid_sid'][_g] == 'stepA1':
            print('hello')
        paths = path_list(group['group'], int(_g))
        for p in paths:
            print('path: {}'.format([group['gid_sid'][_i] for _i in p]))
            print('{}: {}'.format(group['gid_sid'][_g], predict_path_waited(p, group['gid_sid'], waited_expect)))
