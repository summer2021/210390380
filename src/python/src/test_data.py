api_data1 = {
    "api": "CreateVmInstance",
    "body": {
        "instanceOffering": {
            "name": "instanceOffering",
            "cpuNum": 2.0,
            "memorySize": 2097152.0,
            "sortKey": 0.0,
            "type": "UserVm"
        },
        "image": {
            "name": "TinyLinux",
            "url": "http://192.168.1.20/share/images/tinylinux.qcow2",
            "mediaType": "RootVolumeTemplate",
            "architecture": "x86_64",
            "system": False,
            "format": "qcow2",
            "platform": "Linux",
            "backupStorageUuids": [
                "b8fc9c1c027438c28d36af24eca06595"
            ],
            "virtio": False
        },
        "l3Network": {},
        "type": {},
        "rootDiskOffering": {},
        "dataDiskOffering": {},
        "zone": {},
        "cluster": {},
        "host": {},
        "primaryStorage": {},
        "defaultL3Network": {}
    },
    "steps": [{
        "id": 'stepA1',  # added
        "start-time": 1620898736.9057882,  # added
        "name": "prepare-l2-network",
        "sub-steps": [{
            "id": 'stepB1',  # added
            "start-time": 1620898746.9057882,  # added
            "name": "check-ssh-port",
            "sub-steps": []
        }, {
            "id": 'stepB2',  # added
            "start-time": 1620898736.9057882,  # added
            "name": "setup-dhcp-server",
            "sub-steps": []
        }
        ]
    }, {
        "id": 'stepA2',  # added
        "start-time": 1620898756.9057882,  # added
        "name": "install-agent",
        "sub-steps": []
    }, {
        "id": 'stepA3',  # added
        "start-time": 1620898766.9057882,  # added
        "name": "setup-ip-tables",
        "sub-steps": []
    }
    ]
}
api_data2 = {
    "api": "CreateVmInstance",
    "body": {
        "instanceOffering": {
            "name": "instanceOffering",
            "cpuNum": 2.0,
            "memorySize": 2097152.0,
            "sortKey": 0.0,
            "type": "UserVm"
        },
        "image": {
            "name": "TinyLinux",
            "url": "http://192.168.1.20/share/images/tinylinux.qcow2",
            "mediaType": "RootVolumeTemplate",
            "architecture": "x86_64",
            "system": False,
            "format": "qcow2",
            "platform": "Linux",
            "backupStorageUuids": [
                "b8fc9c1c027438c28d36af24eca06595"
            ],
            "virtio": False
        },
        "l3Network": {},
        "type": {},
        "rootDiskOffering": {},
        "dataDiskOffering": {},
        "zone": {},
        "cluster": {},
        "host": {},
        "primaryStorage": {},
        "defaultL3Network": {}
    },
    "steps": [{
        "id": 'stepA1',  # added
        "start-time": 1620898736.9057882,  # added
        "name": "prepare-l2-network",
        "sub-steps": [{
            "id": 'stepB3',  # added
            "start-time": 1620898746.9057882,  # added
            "name": "check-ssh-port",
            "sub-steps": []
        }, {
            "id": 'stepB4',  # added
            "start-time": 1620898736.9057882,  # added
            "name": "setup-dhcp-server",
            "sub-steps": []
        }
        ]
    }, {
        "id": 'stepA2',  # added
        "start-time": 1620898756.9057882,  # added
        "name": "install-agent",
        "sub-steps": []
    }, {
        "id": 'stepA3',  # added
        "start-time": 1620898766.9057882,  # added
        "name": "setup-ip-tables",
        "sub-steps": []
    }
    ]
}
