1. Idea远程调试
iptables -I INPUT -p tcp -m state --state NEW -m tcp --dport 5005 -j ACCEPT

alias mvndebug='mvn -Dmaven.surefire.debug="-Xdebug -Xrunjdwp:transport=dt_socket,server=y,suspend=y,address=5005 -Xnoagent -Djava.compiler=NONE"'

mvndebug test -Dtest=OneVmBasicLifeCycleCase
mvn test -Dtest=OneVmBasicLifeCycleCase -Dapipath
mvndebug test -Dtest=OneVmBasicLifeCycleCase -Dapipath


2. 本地构建
mvn clean install -Dmaven.test.skip=true

3. 测试用例
mvndebug test -Dtest=PredictorCase
mvndebug test -Dtest=SklearnPredictorCase

4. 不打印警告
export skipJacoco=true
然后把test/target目录删除了