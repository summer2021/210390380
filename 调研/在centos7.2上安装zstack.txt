

1. 确认安装下列依赖
yum install -y java-1.8.0-openjdk-devel.x86_64 java-1.8.0-openjdk.x86_64 ant git libffi libffi-devel openssl-devel bc sshpass

2. 安装golang
yum install golang

vim /etc/profile
"""
# GOROOT
export GOROOT=/usr/lib/golang
# GOPATH
export GOPATH=/root/Work/programmer/go/gopath/
# GOPATH bin
export PATH=$PATH:$GOROOT/bin:$GOPATH/bin
"""

source /etc/profile
go version

3. 拉取代码
git clone -b 4.1.3 https://github.com/zstackio/zstack
git clone -b 4.1.3 https://github.com/zstackorg/zstack-utility
git clone -b 4.1.3 https://github.com/zstackorg/zstack-dashboard
wget -c http://archive.apache.org/dist/tomcat/tomcat-7/v7.0.35/bin/apache-tomcat-7.0.35.zip

cd /root/zstack/gitwork/zstack
mvn -DskipTests clean install

cd /root/zstack/gitwork/zstack-utility/zstackbuild

# git checkout1.7.x
# vim build.properties


mvn test -Dtest=OneVmBasicLifeCycleCase
