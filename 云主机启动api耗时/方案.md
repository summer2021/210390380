
#云主机启动进度可视化方案
###1. 给定历史数据(n次创建虚拟机的步骤列表),画出对应的任务步骤有向图
###2. 耗时预估方案一：给定DAG图和开始节点,算出最长等待时间
	最长等待时间=所有路径之时间的最大值(即最耗时的路径).
    示例：
    如根据下述测试数据，
    最长路径为path: ['stepA1', 'stepB4', 'stepB3', 'stepA2', 'stepA3']，耗时30.0s
    作出的DAG图如下：
![avatar](./Figure_1.png)
###3. 耗时预估方案一：给定图和开始节点，按概率计算预估时间
    期望等待时间=求和(路径概率*路径时间).
	路径概率=连乘(节点概率).
	1)节点概率=节点发生频率.
	2)节点概率=函数(时间,总体环境参数,节点发生频率).总体环境参数难以获得.


###历史数据结构（创建虚拟机API）
    创建步骤历史数据示例1：
    api_data = [{
        "api": "CreateVmInstance",
        "body": {
            "instanceOffering": {
                "name": "instanceOffering",
                "cpuNum": 2.0,
                "memorySize": 2097152.0,
                "sortKey": 0.0,
                "type": "UserVm"
            },
            "image": {
                "name": "TinyLinux",
                "url": "http://192.168.1.20/share/images/tinylinux.qcow2",
                "mediaType": "RootVolumeTemplate",
                "architecture": "x86_64",
                "system": False,
                "format": "qcow2",
                "platform": "Linux",
                "backupStorageUuids": [
                    "b8fc9c1c027438c28d36af24eca06595"
                ],
                "virtio": False
            },
            "l3Network": {},
            "type": {},
            "rootDiskOffering": {},
            "dataDiskOffering": {},
            "zone": {},
            "cluster": {},
            "host": {},
            "primaryStorage": {},
            "defaultL3Network": {}
        },
        "steps": [{
            "id": 'stepA1',
            "start-time": 1620898736.9057882,
            "name": "prepare-l2-network",
            "sub-steps": [{
                "id": 'stepB1',
                "start-time": 1620898746.9057882,
                "name": "check-ssh-port",
                "sub-steps": []
            }, {
                "id": 'stepB2',
                "start-time": 1620898736.9057882,
                "name": "setup-dhcp-server",
                "sub-steps": []
            }
            ]
        }, {
            "id": 'stepA2',
            "start-time": 1620898756.9057882,
            "name": "install-agent",
            "sub-steps": []
        }, {
            "id": 'stepA3',
            "start-time": 1620898766.9057882,
            "name": "setup-ip-tables",
            "sub-steps": []
        }
        ]
    }, {
        "api": "CreateVmInstance",
        "body": {
            "instanceOffering": {
                "name": "instanceOffering",
                "cpuNum": 2.0,
                "memorySize": 2097152.0,
                "sortKey": 0.0,
                "type": "UserVm"
            },
            "image": {
                "name": "TinyLinux",
                "url": "http://192.168.1.20/share/images/tinylinux.qcow2",
                "mediaType": "RootVolumeTemplate",
                "architecture": "x86_64",
                "system": False,
                "format": "qcow2",
                "platform": "Linux",
                "backupStorageUuids": [
                    "b8fc9c1c027438c28d36af24eca06595"
                ],
                "virtio": False
            },
            "l3Network": {},
            "type": {},
            "rootDiskOffering": {},
            "dataDiskOffering": {},
            "zone": {},
            "cluster": {},
            "host": {},
            "primaryStorage": {},
            "defaultL3Network": {}
        },
        "steps": [{
            "id": 'stepA1',
            "start-time": 1620898736.9057882,
            "name": "prepare-l2-network",
            "sub-steps": [{
                "id": 'stepB3',
                "start-time": 1620898746.9057882,
                "name": "check-ssh-port",
                "sub-steps": []
            }, {
                "id": 'stepB4',
                "start-time": 1620898736.9057882,
                "name": "setup-dhcp-server",
                "sub-steps": []
            }
            ]
        }, {
            "id": 'stepA2',
            "start-time": 1620898756.9057882,
            "name": "install-agent",
            "sub-steps": []
        }, {
            "id": 'stepA3',
            "start-time": 1620898766.9057882,
            "name": "setup-ip-tables",
            "sub-steps": []
        }
        ]
    }]
