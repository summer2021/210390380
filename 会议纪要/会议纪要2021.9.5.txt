会议纪要2021.9.5
1. 同一个步骤，多线程运算（同一个api请求内，同一个步骤运行了多次，时间不同）
select sum(wait) mwait, count(1) count, apiId, msgName from MsgLogVO group by apiId, msgName order by count desc, mwait desc;
select msgName, taskName, apiId, startTime, replyTime, wait from MsgLogVO where apiId = 'df7e0510dd3540ab97a54f7a2845bf29';

2. 把api参数数值化，再用机器学习算法
  数字：按最大最小值归一化（3，4，6，8，3）-> (3,8) -> (0,1)
  时间：按一天内的时刻归一化（中午12:00:00）-> (一天中的第43200秒) -> (0.5)
  uuid：设为随机变量，按频率归一化（a，b，c，a，c，a） -> (a:3, b:1, c:2) -> (a: 0.5, b: 0.17, c: 0.33)
  其他字符串：取长度，空格数，非英文字符数，阿拉伯数字数，英文字符数作为特征


TODO:
  1. api名字带Get,Query, 不记录进MsgLogVO
  2. api错误的, 在APIHistory中查询，不计入训练集
  3. 步骤数需要作为一个值进行训练
  4. 路径作为中间结果进行训练，需要用到2个模型: api参数->路径，路径->等待时间